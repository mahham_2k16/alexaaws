###################
#	Instructions
###################

To run this project follow the following instructions

1: Clone this project

2: Get into the folder directory from command line i.e. `cd <project directory>`

3: To get the normal stats run `php index.php fetchtopsites`

4: To get all the last 30 days stats run `php index.php fetchtopsites index monthly`

5: Please do run this script on a server as checking for Ads & DFP takes a lot of time.