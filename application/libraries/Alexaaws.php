<?php

defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class AlexaAWS
{

    protected static $TopSitesResponseGroupName = 'Country';
    protected static $TrafficResponseGroupName = 'History';
    protected static $ServiceHost = 'ats.amazonaws.com';
    protected static $NumReturn = 2000;
    protected static $StartNum = 1;
    protected static $CountryCode = 'US';
    protected static $SigVersion = '2';
    protected static $HashAlgorithm = 'HmacSHA256';

    public function __construct( $params ) {
	$this->accessKeyId = $params[ 'accessKeyId' ];
	$this->secretAccessKey = $params[ 'secretAccessKey' ];
    }

    public function index() {
	// empty
    }

    protected static function getTimestamp() {
	return gmdate( "Y-m-d\TH:i:s.\\0\\0\\0\\Z", time() );
    }

    public function buildQueryParams( $action = 'TopSites', $options = array() ) {
		$params = array(
		    'Action' => $action,
		    'AWSAccessKeyId' => $this->accessKeyId,
		    'Timestamp' => self::getTimestamp(),
		    'SignatureVersion' => self::$SigVersion,
		    'SignatureMethod' => self::$HashAlgorithm,
		);

		switch ( $action ) {
		    case 'TopSites':
				$params[ 'Start' ] = isset( $options[ 'start' ] ) ? $options[ 'start' ] : self::$StartNum;
				$params[ 'Count' ] = self::$NumReturn;
				$params[ 'ResponseGroup' ] = self::$TopSitesResponseGroupName;
				$params[ 'CountryCode' ] = isset( $options[ 'country_code' ] ) ? $options[ 'country_code' ] : self::$CountryCode;
				break;
		    case 'TrafficHistory':
				$params[ 'ResponseGroup' ] = self::$TrafficResponseGroupName;
				$params[ 'Range' ] = isset( $options[ 'range' ] ) ? $options[ 'range' ] : 30;
				$params[ 'Url' ] = $options[ 'url' ];
				break;
		    default:
				die( PHP_EOL . "This method is not allowed." . PHP_EOL );
		}

		ksort( $params );
		$keyvalue = array();
		foreach ( $params as $k => $v ) {
		    $keyvalue[] = $k . '=' . rawurlencode( $v );
		}
		return implode( '&', $keyvalue );
    }

    public function makeRequest( $url ) {
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 4 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		$result = curl_exec( $ch );
		curl_close( $ch );
		return $result;
    }

    public function generateSignature( $queryParams ) {
		$sign = "GET\n" . strtolower( self::$ServiceHost ) . "\n/\n" . $queryParams;
		$sig = base64_encode( hash_hmac( 'sha256', $sign, $this->secretAccessKey, true ) );
		return rawurlencode( $sig );
    }

    public function printMessage( $message ) {
		echo "[ " . date( "Y-m-d H:i:s" ) . " ] : " . print_r( $message, 1 ) . PHP_EOL;
    }

    public function xml2array( $xmlObject, $out = array() ) {
		foreach ( ( array ) $xmlObject as $index => $node )
		    $out[ $index ] = ( is_object( $node ) ) ? $this->xml2array( $node ) : $node;

		return $out;
    }

}
