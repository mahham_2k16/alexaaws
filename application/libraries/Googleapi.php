<?php

require_once APPPATH . "/third_party/vendor/autoload.php";

define('APPLICATION_NAME', 'Drive API PHP Quickstart');
define('CREDENTIALS_PATH', 'drive-php-quickstart.json');
define('CLIENT_SECRET_PATH', 'client_secret.json');
define('SCOPES', implode(' ', array(Google_Service_Drive::DRIVE)));

if (php_sapi_name() != 'cli')
    throw new Exception('This application must be run on the command line.');

class Googleapi
{

    public function getClient() {
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfig(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setRedirectUri('http://localhost');

        // code=4/XfxZtk3AUy_7jga8nnaQYkiv45arJ9bibRVbjiCfsO8#

        $authUrl = $client->createAuthUrl();
        $credentialsPath = $this->expandHomeDirectory(CREDENTIALS_PATH);
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
        $client->setAccessToken($accessToken);
        if ($client->isAccessTokenExpired()) {
            try{
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
            }catch(Exception $e){
                die( PHP_EOL . print_r($e->getMessage(),1) . PHP_EOL);
            }
        }
        return $client;
    }

    public function expandHomeDirectory($path) {
        $homeDirectory = getenv('HOME');
        if (empty($homeDirectory)) {
           $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDirectory), $path);
    }

    public function uploadFileOnDrive(){
        if (!file_exists(date('Y-m-d').'.xls')){
            echo "************".PHP_EOL;
            die(PHP_EOL.'Unable to find excel file'.PHP_EOL);
            echo PHP_EOL."************".PHP_EOL;
        }
        $client = $this->getClient();
        $driveService = new Google_Service_Drive($client);
        $fileMetadata = new Google_Service_Drive_DriveFile(array(
            'name' => 'AlexaAWS top sites: ' . date( 'Y-m-d' ),
            'mimeType' => 'application/vnd.google-apps.spreadsheet'));

        $content = file_get_contents(date('Y-m-d').'.xls');
        $file = $driveService->files->create($fileMetadata, array(
            'data' => $content,
            'mimeType' => 'application/vnd.ms-excel',
            'uploadType' => 'multipart',
            'fields' => 'id'));
        $fileId = $file->id;
        $driveService->getClient()->setUseBatch(true);
        try {
            $batch = $driveService->createBatch();
            print "Enter email address on which you want file's ownership: ";
            $owners_email = trim(fgets(STDIN));

            $userPermission = new Google_Service_Drive_Permission(array(
            'type' => 'user',
            'role' => 'owner',
            'emailAddress' => $owners_email
            ));

            $request = $driveService->permissions->create(
                $fileId, $userPermission, array(
                    'fields' => 'id',
                    'transferOwnership'  => true));
            $batch->add($request, 'user');

            $results = $batch->execute();

            foreach ($results as $result) {
            if ($result instanceof Google_Service_Exception) {
                printf($result);
            }
            }
        }
        finally {
            $driveService->getClient()->setUseBatch(false);
        }
    } 
    
}
