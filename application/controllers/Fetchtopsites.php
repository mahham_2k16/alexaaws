<?php

defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class FetchTopSites extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->library( 'alexaaws', array(
		    'accessKeyId' => 'AKIAIDXCIMMNLANCRNSQ',
		    'secretAccessKey' => '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' ), 'AlexaAWS' );
		$this->load->library('excel');
		$this->load->library('googleapi');

    }

    public function index( $monthly = '' ) {
		$countries = array( 'US', 'GB', 'BR', 'FR', 'DE', 'SE', 'NL', 'CA' );
		foreach ( $countries as $k => $country ) {
    		$this->excel->createSheet();
			for( $i = 0; $i < 20; $i++ )
			{
			    $queryParams = $this->AlexaAWS->buildQueryParams( 'TopSites', array( 'country_code' => $country, 'start' => ( ( $i * 100 ) + 1 ) ) );
			    $sig = $this->AlexaAWS->generateSignature( $queryParams );
			    $this->AlexaAWS->printMessage('http://ats.amazonaws.com/?' . $queryParams . '&Signature=' . $sig );
				$ret = $this->AlexaAWS->makeRequest( 'http://ats.amazonaws.com/?' . $queryParams . '&Signature=' . $sig );
				$this->populate_xls( $k, $country, $this->parse_top_sites( $ret, $monthly ), ( ( $i * 100 ) + 1 ) );
			}
		}

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
	    $objWriter->save( date( 'Y-m-d' ) . '.xls' );
	    $this->googleapi->uploadFileOnDrive();
	    $this->AlexaAWS->printMessage( 'File has been shared with your given email.' );
    }

    private function parse_top_sites( $response, $monthly = '' ) {
		$count = 0;
		$results = array();
		$xml = new SimpleXMLElement( $response, null, false, 'http://ats.amazonaws.com/doc/2005-11-21' );
		foreach ( $xml->Response->TopSitesResult->Alexa->TopSites->Country->Sites->children( 'http://ats.amazonaws.com/doc/2005-11-21' ) as $site ) {
		    $site = $this->AlexaAWS->xml2array( $site );
		    $this->AlexaAWS->printMessage( $site[ 'DataUrl' ] );
		    $ads_status = $this->check_for_google_ads( $site[ 'DataUrl' ] );
		    $site = array_merge( $site, $ads_status );
		    if ( strcmp( $monthly, 'monthly' ) === 0 )
			$results[] = array_merge( $site, array( 'Monthly' => $this->get_monthly_views( $site[ 'DataUrl' ] ) ) );
		    else
			$results[] = $site;
		}

		return $results;
    }

    private function populate_xls( $sheet, $country, $data, $i ) {
    	$this->excel->setActiveSheetIndex( $sheet );
		$this->excel->getActiveSheet()->setTitle( $country );
		$this->set_sheet_titles();

		$i += 1;
		foreach ($data as $row) {
			$this->excel->getActiveSheet()->setCellValue("A{$i}",  $this->get_value( $row['DataUrl'] ) );
			$this->excel->getActiveSheet()->setCellValue("B{$i}",  $this->get_value( $row['Country']['Rank'] ) );
			$this->excel->getActiveSheet()->setCellValue("C{$i}",  $this->get_value( $row['Global']['Rank'] ) );
			$this->excel->getActiveSheet()->setCellValue("G{$i}",  $this->get_value( $row['AdSense (Y/N)'] ) );
			$this->excel->getActiveSheet()->setCellValue("H{$i}",  $this->get_value( $row['DFP (Y/N)'] ) );
			$this->excel->getActiveSheet()->setCellValue("D{$i}",  $this->get_value( $row['Country']['Reach']['PerMillion'] ) );

			if ( !isset( $row['Monthly'] ) ) {
			$this->excel->getActiveSheet()->setCellValue("E{$i}",  $this->get_value( $row['Country']['PageViews']['PerMillion'] ) );
			$this->excel->getActiveSheet()->setCellValue("F{$i}",  $this->get_value( $row['Country']['PageViews']['PerUser'] ) );
			$this->excel->getActiveSheet()->setCellValue("I{$i}",  $this->get_value( date('j M, Y') ) );
			} else {
				foreach ($row['Monthly'] as $date) {
					$this->excel->getActiveSheet()->setCellValue("A{$i}",  $this->get_value( $row['DataUrl'] ) );
					$this->excel->getActiveSheet()->setCellValue("B{$i}",  $this->get_value( $row['Country']['Rank'] ) );
					$this->excel->getActiveSheet()->setCellValue("C{$i}",  $this->get_value( $row['Global']['Rank'] ) );
					$this->excel->getActiveSheet()->setCellValue("G{$i}",  $this->get_value( $row['AdSense (Y/N)'] ) );
					$this->excel->getActiveSheet()->setCellValue("H{$i}",  $this->get_value( $row['DFP (Y/N)'] ) );
					$this->excel->getActiveSheet()->setCellValue("D{$i}",  $this->get_value( $row['Country']['Reach']['PerMillion'] ) );
					$this->excel->getActiveSheet()->setCellValue("E{$i}",  $this->get_value( $date['PageViews']['PerMillion'] ) );
					$this->excel->getActiveSheet()->setCellValue("F{$i}",  $this->get_value( $date['PageViews']['PerUser'] ) );
					$this->excel->getActiveSheet()->setCellValue("I{$i}",  $this->get_value( date('j M, Y', strtotime($date['Date'])) ) );
					$i++;
				}
			}
			$i++;
		}
    }

    private function get_value( $value ) {
    	return isset( $value ) ? $value : 'N/A';
    }

    private function set_sheet_titles() {

		$this->excel->getActiveSheet()->setCellValue('A1', 'URL');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('B1', 'Rank ( Country )');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('C1', 'Rank ( Global )');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('D1', 'Reach ( Country )');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('E1', 'Page Views ( PerMillion ) ');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('F1', 'Page Views ( PerUser ) ');
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('G1', 'AdSense ( Y/N )');
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('H1', 'DFP ( Y/N )');
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize(13)->setBold(true);
		$this->excel->getActiveSheet()->setCellValue('I1', 'Date');
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize(13)->setBold(true);
    }

    private function get_monthly_views( $url = '' ) {
		$results = array();
		if ( strlen( $url ) > 0 ) {
		    $queryParams = $this->AlexaAWS->buildQueryParams( 'TrafficHistory', array( 'url' => $url ) );
		    $sig = $this->AlexaAWS->generateSignature( $queryParams );
		    $ret = $this->AlexaAWS->makeRequest( 'http://ats.amazonaws.com/?' . $queryParams . '&Signature=' . $sig );
		    $results = $this->parse_monthly_response( $ret );
		}

		return $results;
    }

    private function parse_monthly_response( $response ) {
		$perDate = array();
		$xml = new SimpleXMLElement( $response, null, false, 'http://awis.amazonaws.com/doc/2005-07-11' );
		foreach ( $xml->Response->TrafficHistoryResult->Alexa->TrafficHistory->HistoricalData->children( 'http://awis.amazonaws.com/doc/2005-07-11' ) as $data ) {
		    $perDate[] = $this->AlexaAWS->xml2array( $data );
		}

		return $perDate;
    }

    private function check_for_google_ads( $url = '' ) {
		$ads = array( 'AdSense (Y/N)' => 'N', 'DFP (Y/N)' => 'N' );
		if ( strlen( $url ) > 0 ) {
		    $site = @file_get_contents( ( strstr( $url, 'http://' ) ) ? $url : ('http://' . $url)  );
		    if ( strstr( $site, 'ca-pub-' ) )
		    	$ads['AdSense (Y/N)'] = 'Y';
	    	else
	    		$ads['AdSense (Y/N)'] = 'N';


		    if ( strstr( $site, 'googletag.cmd' ) )
		    	$ads['DFP (Y/N)'] = 'Y';
	    	else
	    		$ads['DFP (Y/N)'] = 'N';
		}

		return $ads;
    }

}
